import logging
from xquanto_python.run import XRun
from deside import XDecisionSystem


rest_url = "https://api.bitboardexchange.com/v1"
ws_uri = "wss://bitboardexchange.com/v1/ws"
# wss_uri = "ws://localhost:8000/ws"
# wss_uri_private = "wss://www.bitmex.com/"



api_key = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjo2LCJjcmVhdGVkIjoiMjAyMS0wMy0yMFQxOTozMTowNy4yMTI3OTQ5OVoiLCJleHBpcmF0aW9uIjoiMDAwMS0wMS0wMVQwMDowMDowMFoiLCJicm9rZXIiOiJIRlRCIiwicm9sZSI6IkNMSUVOVCIsInVzZXJfaWQiOjEwMDAwMiwibmFtZSI6ImtleTMyIn0.UDbsiTRftTvc41KsXpkmbYjIGBM8qy3F9veKdb5mNmovD_s9mfimrRhW0q_q2lS8iXv1a3iyXH4I5vdscgXVQw"
api_secret = "WWGvsZHyFDFKWJbf67sLGC"
account_guid = "e8e93493-a1ca-4785-a16f-4617e89d71f7"


MIN_MOVE = {
    "BTCUSD-ZZ": 0.5,
    "ETHUSD-ZZ": 0.1,
}


def setup_logger():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    logging._defaultFormatter = logging.Formatter(u"%(message)s")
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


logger = setup_logger()
dss = XDecisionSystem(logger, account_guid, MIN_MOVE)
run = XRun(logger, api_key, api_secret, account_guid, rest_url, ws_uri, dss)
run.run_forever()
