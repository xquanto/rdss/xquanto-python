# xquanto

Расширенная библиотека для доступа к REST-API, WebSocket-API и пример создания 
робастной системы принятия решений (Robust Decision Support System)

## License

- MIT

## Documentation


## Contributing

Please see the contribution guidelines at
[https://websocket-client.readthedocs.io/en/latest/contributing.html](https://websocket-client.readthedocs.io/en/latest/contributing.html)

## Installation

First, install the following dependencies:
- six
- backports.ssl\_match\_hostname for Python 2.x

You can install the dependencies with the command `pip install six` and
`pip install backports.ssl_match_hostname`

You can use either `python setup.py install` or `pip install websocket-client`
to install. This module is tested on Python 2.7 and Python 3.4+. Python 3
support was first introduced in version 0.14.0, but is a work in progress.
