try:
    import simplejson as json
except ImportError:
    import json


class XSubscirbe(object):
    def _subscribe(self, args) -> str:
        message = {
            "op": "subscribe",
            "args": args,
        }
        return json.dumps(message)

    def _unsubscribe(self, args) -> str:
        message = {
            "op": "unsubscribe",
            "args": args,
        }
        return json.dumps(message)

    def _args(self, topic, account_guid) -> []:
        arg = {
            "topic": topic,
            "account_guid": self.account_guid,
        }
        return [arg]

    def _account_symbol_args(self, topic, account_guid, symbols) -> []:
        args = []
        for symbol in symbols:
            arg = {
                "topic": topic,
                "account_guid": self.account_guid,
                "symbol": symbol
            }
            args.append(arg)
        return args

    def _symbol_args(self, topic, symbols) -> []:
        args = []
        for symbol in symbols:
            arg = {
                "topic": topic,
                "symbol": symbol
            }
            args.append(arg)
        return args

    def _symbol_timeframe_args(self, topic, symbols, timeframe) -> []:
        args = []
        for symbol in symbols:
            arg = {
                "topic": topic,
                "symbol": symbol,
                "timeframe": timeframe,
            }
            args.append(arg)
        return args


class XSubscirbePrivate(XSubscirbe):
    def __init__(self, account_guid):
        self.account_guid = account_guid

    def subscribe_portfolio(self) -> str:
        args = self._args("limit_portfolios", self.account_guid)
        return self._subscribe(args)

    def subscribe_bundles(self, cross_symbols) -> str:
        args = self._account_symbol_args("limit_bundles",
                                         self.account_guid, cross_symbols)
        return self._subscribe(args)

    def subscribe_positions(self, contract_symbols) -> str:
        args = self._account_symbol_args("limit_positions",
                                         self.account_guid, contract_symbols)
        return self._subscribe(args)

    def subscribe_orders(self, contract_symbols) -> str:
        args = self._account_symbol_args("limit_orders",
                                         self.account_guid, contract_symbols)
        return self._subscribe(args)

    def subscribe_executions(self, contract_symbols) -> str:
        args = self._account_symbol_args("limit_executions",
                                         self.account_guid, contract_symbols)
        return self._subscribe(args)

    def subscribe_allocated(self, contract_symbols) -> str:
        args = self._args("limit_allocated_positions", self.account_guid)
        return self._subscribe(args)

    def subscribe_activated(self, contract_symbols) -> str:
        args = self._args("limit_activated_positions", self.account_guid)
        return self._subscribe(args)

    def unsubscribe_portfolio(self) -> str:
        args = self._args("limit_portfolios", self.account_guid)
        return self._unsubscribe(args)

    def unsubscribe_bundles(self, cross_symbols) -> str:
        args = self._account_symbol_args("limit_bundles",
                                         self.account_guid, cross_symbols)
        return self._unsubscribe(args)

    def unsubscribe_positions(self, contract_symbols) -> str:
        args = self._account_symbol_args("limit_positions",
                                         self.account_guid, contract_symbols)
        return self._unsubscribe(args)

    def unsubscribe_orders(self, contract_symbols) -> str:
        args = self._account_symbol_args("limit_orders",
                                         self.account_guid, contract_symbols)
        return self._unsubscribe(args)

    def unsubscribe_executions(self, contract_symbols) -> str:
        args = self._account_symbol_args("limit_executions",
                                         self.account_guid, contract_symbols)
        return self._unsubscribe(args)

    def unsubscribe_allocated(self, contract_symbols) -> str:
        args = self._args("limit_allocated_positions", self.account_guid)
        return self._unsubscribe(args)

    def unsubscribe_activated(self, contract_symbols) -> str:
        args = self._args("limit_activated_positions", self.account_guid)
        return self._unsubscribe(args)


class XSubscirbePublic(XSubscirbe):
    def subscribe_scores(self, contract_symbols) -> str:
        args = self._symbol_args("mark_scores", contract_symbols)
        return self._subscribe(args)

    def subscribe_greeks(self, contract_symbols) -> str:
        args = self._symbol_args("mark_greeks", contract_symbols)
        return self._subscribe(args)

    def subscribe_candles(self, contract_symbols, timeframe) -> str:
        args = self._symbol_timeframe_args("mark_candles",
                                           contract_symbols, timeframe)
        return self._subscribe(args)

    def subscribe_trade_candles(self, contract_symbols, timeframe) -> str:
        args = self._symbol_timeframe_args("mark_trade_candles",
                                           contract_symbols, timeframe)
        return self._subscribe(args)

    def subscribe_spreads(self, contract_symbols) -> str:
        args = self._symbol_args("matcher_spreads", contract_symbols)
        return self._subscribe(args)

    def subscribe_last_trades(self, contract_symbols) -> str:
        args = self._symbol_args("matcher_last_trades", contract_symbols)
        return self._subscribe(args)

    def subscribe_glass_partials(self, contract_symbols) -> str:
        args = self._symbol_args("matcher_glass_partials", contract_symbols)
        return self._subscribe(args)
