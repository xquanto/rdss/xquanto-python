import websocket
import ssl

from .auth import header


class XWebSocket(object):
    def __init__(self, logger, api_key, api_secret,
                 account_guid, uri, dss):
        websocket.enableTrace(True)
        self.logger = logger
        self.dss = dss
        self.ws = websocket.WebSocketApp(
            uri,
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close,
            on_open=self.on_open,
            header=header(api_key, api_secret, "GET", uri, "")
        )

    def is_connected(self):
        return self.ws.sock and self.ws.sock.connected

    def run_forever(self):
        self.ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE}, origin="")

    def on_open(self):
        self.logger.info("xquanto websocket opened")

    def on_error(self, error):
        self.logger.error("error: %s", error)

    def on_close(self):
        self.logger.info("xquanto websocket closed")

    def close(self):
        if self.is_connected():
            self.logger.info("xquanto websocket closing...")
            self.ws.close()

    def on_message(self, message):
        self.dss.append_message(message)
