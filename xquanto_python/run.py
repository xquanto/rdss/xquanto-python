import time
import websocket
import threading

from .ws import XWebSocket
from .rest import XRest
from .subscribe import XSubscirbePrivate, XSubscirbePublic


class XRun(object):
    def __init__(self, logger, api_key, api_secret,
                 account_guid, rest_url, ws_uri, dss):
        websocket.enableTrace(True)

        self.logger = logger
        self.dss = dss

        self.rest = XRest(self.logger, api_key,
                          api_secret, account_guid, rest_url)
        self.ws = XWebSocket(self.logger, api_key,
                             api_secret, account_guid, ws_uri, self.dss)


        self.dss.append_rest(self.rest)
        self.sub_private = XSubscirbePrivate(account_guid)
        self.sub_public = XSubscirbePublic()
        self.wst = None

    def run_forever(self):
        self.wst = threading.Thread(target=lambda: self.ws.run_forever())
        self.wst.daemon = True
        self.wst.start()
        time.sleep(2)

        self.prepare()
        self.subscribe()
        while self.ws.is_connected():
            try:
                time.sleep(1)
                self.dss.deside()
                new_orders = self.dss.new_orders("BTCUSD-ZZ")
                cancel_orders = self.dss.cancel_orders("BTCUSD-ZZ")

                if new_orders:
                    self.rest.place_multi_orders(new_orders)
                if cancel_orders:
                    self.rest.cancel_multi_orders("BTCUSD-ZZ", cancel_orders)
            except websocket.WebSocketConnectionClosedException:
                break
            except KeyboardInterrupt:
                break
        print("thread is closing...")
        self.close()

    def send_ws(self, message):
        self.ws.ws.send(message)

    def prepare(self):
        orders, status = self.rest.get_orders()
        if status:
            self.logger.info(orders)
            self.dss.append_orders(orders)
        else:
            self.close()

        glass, status = self.rest.get_glass("BTCUSD-ZZ")
        if status:
            self.logger.info(glass)
            self.dss.append_glass(glass, False)
        else:
            self.close()

    def subscribe(self):
        try:
            self.logger.info("subscribe")
            # setup subscribes
            message = self.sub_private.subscribe_portfolio()
            self.send_ws(message)

            message = self.sub_private.subscribe_orders(["BTCUSD-ZZ"])
            self.send_ws(message)

            message = self.sub_public.subscribe_trade_candles(
                ["BTCUSD-ZZ"], "m5")
            self.send_ws(message)

            message = self.sub_public.subscribe_spreads(["BTCUSD-ZZ"])
            self.send_ws(message)

            message = self.sub_public.subscribe_glass_partials(["BTCUSD-ZZ"])
            self.send_ws(message)
        except websocket.WebSocketConnectionClosedException:
            # ws_public.close()
            self.close()

    def close(self):
        self.ws.close()
        time.sleep(1)
        self.wst.join()
