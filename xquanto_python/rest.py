import requests
from .auth import XAuth

try:
    import simplejson as json
except ImportError:
    import json

TIMEOUT = 10


class XRest(object):
    def __init__(self, logger, api_key, api_secret, account_guid, url):
        self.logger = logger
        self.auth = XAuth(api_key, api_secret)
        self.account_guid = account_guid
        self.url = url
        self.session = requests.Session()
        self.session.headers.update({"user-agent": "xquanto-example"})

    def get_crosses(self):
        resp, status = self.make_request(path="/pub/cross", verb="GET")
        return resp.get("crosses", []), status == "success"

    def get_contracts(self, filter_query):
        resp, status = self.make_request(path="/pub/contract",
                                         query=filter_query, verb="GET")
        return resp.get("contracts", []), status == "success"

    def get_portfolio(self):
        filter_query = {
            "broker": "",
            "account_guid": self.account_guid,
        }
        resp, status = self.make_request(path="/client/current_portfolio",
                                         query=filter_query, verb="GET")
        return resp.get("portfolio", {}), status == "success"

    def get_allocated(self):
        filter_query = {
            "broker": "",
            "account_guid": self.account_guid,
        }
        resp, status = self.make_request(
            path="/client/portfolio_allocated_position",
            query=filter_query, verb="GET")
        return resp.get("contracts", {}), status == "success"

    def get_activated(self):
        filter_query = {
            "broker": "",
            "account_guid": self.account_guid,
        }
        resp, status = self.make_request(
            path="/client/portfolio_activated_position",
            query=filter_query, verb="GET")
        return resp.get("contracts", {}), status == "success"

    def get_bundles(self, cross_symbols):
        filter_query = {
            "broker": "",
            "account_guid": self.account_guid,
            "cross_symbols": cross_symbols,
        }
        resp, status = self.make_request(path="/client/current_bundle",
                                         query=filter_query, verb="GET")
        return resp.get("bundles", []), status == "success"

    def get_positions(self, contract_symbols):
        filter_query = {
            "broker": "",
            "account_guid": self.account_guid,
            "contract_symbols": contract_symbols,
        }
        resp, status = self.make_request(path="/client/current_position",
                                         query=filter_query, verb="GET")
        return resp.get("positions", []), status == "success"

    def get_orders(self):
        filter_query = {
            "broker": "",
            "account_guid": self.account_guid,
        }
        resp, status = self.make_request(path="/client/current_order",
                                         query=filter_query, verb="GET")
        return resp.get("orders", []), status == "success"

    def get_chart(self, filter_query):
        resp, status = self.make_request(path="/pub/chart",
                                         query=filter_query, verb="GET")
        return resp.get("chart", {}), status == "success"

    def get_trade_chart(self, filter_query):
        resp, status = self.make_request(path="/pub/trade_chart",
                                         query=filter_query, verb="GET")
        return resp.get("chart", {}), status == "success"

    def get_spread(self, contract_symbol):
        cross_symbol = contract_symbol.split("-")[0]
        path = f"/pub/get_spread/{cross_symbol}/{contract_symbol}"
        resp, status = self.make_request(path=path, verb="GET")
        return resp.get("spread", {}), status == "success"

    def get_glass(self, contract_symbol):
        cross_symbol = contract_symbol.split("-")[0]
        path = f"/pub/timesale_get_levelii/{cross_symbol}/{contract_symbol}"
        resp, status = self.make_request(path=path, verb="GET")
        return resp.get("glass", {}), status == "success"

    def get_scores(self, index_symbols):
        filter_query = {
            "index_symbols": index_symbols,
        }
        resp, status = self.make_request(path="/pub/index_scores",
                                         query=filter_query, verb="GET")
        return resp.get("scores", []), status == "success"

    def get_greeks(self, contract_symbols):
        filter_query = {
            "contract_symbols": contract_symbols,
        }
        resp, status = self.make_request(path="/pub/greeks",
                                         query=filter_query, verb="GET")
        return resp.get("greeks", []), status == "success"

    def place_multi_orders(self, multi_orders):
        resp, status = self.make_request(path="/client/multi_order",
                                         payload=multi_orders, verb="PUT")
        return resp.get("orders", []), status == "success"

    def cancel_multi_orders(self, contract_symbol, order_guids):
        filter_query = {
            "account_guid": self.account_guid,
            "contract_symbol": contract_symbol,
            "order_guids": order_guids,
        }
        resp, status = self.make_request(path="/client/multi_order",
                                         query=filter_query, verb="DELETE")
        return resp.get("cancel", []), status == "success"

    def make_request(self, path, query=None, payload=None, verb=None):
        url = self.url + path

        try:
            req_info = json.dumps(payload or query or "")
            self.logger.debug(f"sending {verb} req to {url}: {req_info}")

            req = requests.Request(verb, url, json=payload,
                                   auth=self.auth, params=query)
            prepped = self.session.prepare_request(req)
            response = self.session.send(prepped, timeout=TIMEOUT)
            response.raise_for_status()

        except requests.exceptions.HTTPError as e:
            if response is None:
                raise e

            status = response.json().get("status", "")
            self.logger.error("error: " + response.text)
            return {}, status

        except requests.exceptions.Timeout as e:
            self.logger.warning("timeout, try retry after 5 seconds")
            raise e

        except requests.exceptions.ConnectionError as e:
            self.logger.warning("connection error, try retry after 5 seconds")
            raise e

        self.logger.debug(response.json())
        return response.json(), "success"  # status_code is 200
