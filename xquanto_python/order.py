class XMultiOrder(object):
    """XMultiOrder"""

    def __init__(self, account_guid):
        self.account_guid = account_guid
        self.orders = []

    def append_limit(self, price, quantity, side):
        order = {
            "order_type": "ORDER_LIMIT",
            "side": side,
            "quantity": quantity,
            "price": price,
            "time_in_force": "ORDERTTL_GTC",
        }
        self.orders.append(order)

    def append_post(self, price, quantity, side):
        order = {
            "order_type": "ORDER_LIMIT",
            "side": side,
            "quantity": quantity,
            "price": price,
            "exec_inst": "EXECINST_POST",
            "time_in_force": "ORDERTTL_GTC",
        }
        self.orders.append(order)

    def append_market(self, quantity, side):
        order = {
            "order_type": "ORDER_MARKET",
            "side": side,
            "quantity": quantity,
            "time_in_force": "ORDERTTL_GTC",
        }
        self.orders.append(order)

    def dump(self, contract_symbol):
        if len(self.orders) > 0:
            dump = {
                "account_guid": self.account_guid,
                "contract_symbol": contract_symbol,
                "orders": self.orders,
            }
        else:
            dump = None
        return dump
