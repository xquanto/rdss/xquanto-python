import hashlib
import hmac
import urllib
import time
from requests.auth import AuthBase

# # dev
# API_KEY = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjo2LCJjcmV" \
#     "hdGVkIjoiMjAyMS0wMi0wOFQxMDo0NTo1MS4zOTc3NzYwMTRaIiwiZXhwaXJhdGlvbiI6I" \
#     "jAwMDEtMDEtMDFUMDA6MDA6MDBaIiwiYnJva2VyIjoiSEZUQiIsInJvbGUiOiJLRUVQRVIi" \
#     "LCJ1c2VyX2lkIjoyLCJuYW1lIjoic2thbHBlciJ9.6YSpu37MX9tQvUWOjThIC756AW6RJL" \
#     "dMFyxYsLuCaCRh6a7pxUL45qqx7qY29XpD57fEzgvqDlB_-FbXeqrOmw"
#
# API_SECRET = "AgunJ5SrAUd1wF7fdMwm4X"


# master
# API_KEY = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjo2LCJjcmV" \
#     "hdGVkIjoiMjAyMS0wMS0yMVQxNDoxNzoyMS4zMzI1MDk1OTdaIiwiZXhwaXJhdGl" \
#     "vbiI6IjAwMDEtMDEtMDFUMDA6MDA6MDBaIiwiYnJva2VyIjoiSEZUQiIsInJvbGU" \
#     "iOiJDTElFTlQiLCJ1c2VyX2lkIjoxLCJuYW1lIjoia2V5MzYifQ.jtZACN_Yu5Qk" \
#     "qwuG5ej7i7SKGHVaIZoNYISgO_EEVKlQdfTWzib0wjUVCLw6A1Iz-ueBS7wh7dImq" \
#     "YtJFVTchg"
#
# API_SECRET = "GW58kVhBoHtdFbndMezEs6"

# local
# API_KEY = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjo2LCJjcmVhdGVkIjoiMjAyMS0wMi0wOFQxNzoyNjo0My4xMjk5NDkrMDM6MDAiLCJleHBpcmF0aW9uIjoiMDAwMS0wMS0wMVQwMDowMDowMFoiLCJicm9rZXIiOiJIRlRCIiwicm9sZSI6IkNMSUVOVCIsInVzZXJfaWQiOjEwMDAwMCwibmFtZSI6ImtleTMyIn0.iyVmdilEQyfuYtZ9BjFdhoK1CUfUHTmHnnVsWi_bLUvPJxFEWoQz23AyMfqNKM6ozLf7AK2kTrLFNuFD55N9QA"
# API_SECRET = "S6d9f5xJzwgkV8Ege7ez8Y"


def generate_signature(secret, verb, uri, expires, body):
    parsedURL = urllib.parse.urlparse(uri)
    path = parsedURL.path
    if parsedURL.query:
        path = path + '?' + parsedURL.query
    body = body.decode("utf8") if isinstance(body, bytes) else body
    message = (verb + path + expires + body).encode('utf-8')
    signature = hmac.new(
        secret.encode('utf-8'),
        message, digestmod=hashlib.sha256).hexdigest()
    return signature


def header(api_key, api_secret, method, uri, body):
    expires = str(int(time.time() + 5))
    signature = generate_signature(api_secret, method, uri, expires, body)
    return {
        "X-Api-Expires": expires,
        "X-Api-Signature": signature,
        "X-Api-Key": api_key
    }


class XAuth(AuthBase):
    def __init__(self, api_key, api_secret):
        self.api_key = api_key
        self.api_secret = api_secret

    def __call__(self, r):
        h = header(self.api_key,
                   self.api_secret, r.method, r.url, r.body or "")
        r.headers.update(h)
        return r
