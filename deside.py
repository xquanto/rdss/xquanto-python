from xquanto_python.order import XMultiOrder
from threading import Lock

try:
    import simplejson as json
except ImportError:
    import json


ORDER_ACTIVE = [
    "ORDERSTATUS_NEW",
    "ORDERSTATUS_PARTIAL",
]


def round_price(price, min_move) -> float:
    return round(price / min_move) / (1 / min_move)


# message GlassQuoteDump {
#     // цена котировки
#     required double price
#     // количество контрактов
#     required int64 quantity
# }

# message GlassPartialSideDump {
#     // обновления в стакане
#     repeated GlassQuoteDump news
#     // изменения в стакане
#     repeated GlassQuoteDump updates
#     // удаления в стакане
#     repeated double deletes
# }

# message GlassPartialDump {
#     // символ контракта
#     // @inject_tag: validate:"max=20"
#     required string contract_symbol
#     // покупатели
#     required GlassPartialSideDump bid
#     // продавцы
#     required GlassPartialSideDump ask
#     // лучшая цена покупки
#     optional double bid_price
#     // количество на лучшей цене покупки
#     required int64 bid_quantity
#     // лучшая цена продажи
#     optional double ask_price
#     // количество на лучшей цене продажи
#     required int64 ask_quantity
#     // центральное время
#     required google.protobuf.Timestamp central_time
#     // предыдущее центральное время
#     required google.protobuf.Timestamp prev_central_time
# }

class XDecisionSystem(object):
    def __init__(self, logger, account_guid, min_move):
        self.logger = logger
        self.account_guid = account_guid
        self.spread_map = {}
        self.glass_map = {}
        self.order_map = {}
        self.min_move = min_move
        self.rest = None
        self.mutex = Lock()

    def append_message(self, message):
        # self.logger.debug(message.decode("utf-8"))
        msg = json.loads(message)
        if msg.get("topic") == "matcher_spreads":
            self.append_spread(msg["update"])
        elif msg.get("topic") == "matcher_glass_partials":
            self.append_glass_partial(msg["update"])
        elif msg.get("topic") == "limit_orders":
            self.append_orders(msg["update"])

    def append_rest(self, rest):
        self.rest = rest

    def append_spread(self, spread):
        with self.mutex:
            self.spread_map[spread["contract_symbol"]] = spread

    def append_glass_partial(self, glass_partial):
        contract_symbol = glass_partial["contract_symbol"]
        bid_inserts = glass_partial["bid"].get("news", [])
        ask_inserts = glass_partial["ask"].get("news", [])
        bid_updates = glass_partial["bid"].get("updates", [])
        ask_updates = glass_partial["ask"].get("updates", [])
        bid_deletes = glass_partial["bid"].get("deletes", [])
        ask_deletes = glass_partial["ask"].get("deletes", [])
        central_time = glass_partial["central_time"]
        prev_central_time = glass_partial["prev_central_time"]
        with self.mutex:
            glass = self.glass_map.get(contract_symbol, {})
            if glass.get("central_time") is None:
                self.logger.warn("glass is not ready for: " + contract_symbol)
                if self.rest:
                    glass, status = self.rest.get_glass(contract_symbol)
                    if status:
                        self.logger.info(glass)
                        self.dss.append_glass(glass, True)
            elif glass["central_time"] != prev_central_time:
                self.logger.error(
                    "central time is mismatch: %s, %s",
                    glass["central_time"], prev_central_time)
            else:
                for insert in bid_inserts:
                    if glass["bids"].get(insert["price"]) is not None:
                        self.logger.error(
                            "glass insert bid price exists: %s@%f",
                            contract_symbol, insert["price"])
                    else:
                        glass["bids"][insert["price"]] = insert["quantity"]

                for insert in ask_inserts:
                    if glass["asks"].get(insert["price"]) is not None:
                        self.logger.error(
                            "glass insert ask price exists: %s@%f",
                            contract_symbol, insert["price"])
                    else:
                        glass["asks"][insert["price"]] = insert["quantity"]

                for update in bid_updates:
                    if glass["bids"].get(update["price"]) is None:
                        self.logger.error(
                            "glass update bid price not exists: %s@%f",
                            contract_symbol, update["price"])
                    else:
                        glass["bids"][update["price"]] += update["quantity"]

                for update in ask_updates:
                    if glass["asks"].get(update["price"]) is None:
                        self.logger.error(
                            "glass update ask price not exists: %s@%f",
                            contract_symbol, update["price"])
                    else:
                        glass["asks"][update["price"]] += update["quantity"]

                for delete in bid_deletes:
                    if glass["bids"].get(delete) is None:
                        self.logger.error(
                            "glass delete bid price not exists: %s@%f",
                            contract_symbol, delete)
                    else:
                        del(glass["bids"][delete])

                for delete in ask_deletes:
                    if glass["asks"].get(delete) is None:
                        self.logger.error(
                            "glass delete ask price not exists: %s@%f",
                            contract_symbol, delete)
                    else:
                        del(glass["asks"][delete])

                glass["central_time"] = central_time

    def append_glass(self, glass, is_locked):
        if is_locked:
            self._append_glass(glass["contract_symbol"], glass["bids"],
                               glass["asks"], glass["central_time"])
        else:
            with self.mutex:
                self._append_glass(glass["contract_symbol"], glass["bids"],
                                   glass["asks"], glass["central_time"])

    def _append_glass(self, contract_symbol, bids, asks, central_time):
        glass = {"bids": {}, "asks": {}}
        for bid in bids:
            glass["bids"][bid["price"]] = bid["quantity"]

        for ask in asks:
            glass["asks"][ask["price"]] = ask["quantity"]
        glass["central_time"] = central_time

        self.glass_map[contract_symbol] = glass

    def append_orders(self, orders):
        for order in orders:
            order_guid = order["order_guid"]
            contract_symbol = order["contract_symbol"]
            status = order["status"]
            if self.order_map.get(contract_symbol) is None:
                self.order_map[contract_symbol] = {}

            self.order_map[contract_symbol][order_guid] = order

            if status not in ORDER_ACTIVE:
                del self.order_map[contract_symbol][order_guid]

    def deside(self):
        self.logger.info("deside")

    def new_orders(self, contract_symbol):
        orders = self.order_map.get(contract_symbol, {})
        bid_orders = list(filter(lambda x: x["side"] == "SIDE_BUY",
                                 orders.values()))
        ask_orders = list(filter(lambda x: x["side"] == "SIDE_SELL",
                                 orders.values()))
        spread = self.spread_map.get(contract_symbol, {})
        bid = spread.get("bid")
        ask = spread.get("ask")

        multi_order = XMultiOrder(self.account_guid)
        if bid is not None and len(bid_orders) < 2:
            price = round_price(bid * 0.999, self.min_move[contract_symbol])
            multi_order.append_limit(price, 1, "SIDE_BUY")
        if ask is not None and len(ask_orders) < 2:
            price = round_price(ask * 1.001, self.min_move[contract_symbol])
            multi_order.append_limit(price, 1, "SIDE_SELL")

        return multi_order.dump(contract_symbol)

    def cancel_orders(self, contract_symbol):
        orders = self.order_map.get(contract_symbol, {})
        bid_orders = filter(lambda x: x["side"] == "SIDE_BUY",
                            orders.values())
        ask_orders = filter(lambda x: x["side"] == "SIDE_SELL",
                            orders.values())
        bid_orders = sorted(bid_orders,
                            key=lambda x: x["price"], reverse=True)
        ask_orders = sorted(ask_orders,
                            key=lambda x: x["price"], reverse=False)

        cancel_guids = []
        len_bid_orders = len(bid_orders)
        len_ask_orders = len(ask_orders)
        if len_bid_orders > 2:
            cancel_guids.append(bid_orders[-1]["order_guid"])
        if len_ask_orders > 2:
            cancel_guids.append(ask_orders[-1]["order_guid"])

        return cancel_guids
